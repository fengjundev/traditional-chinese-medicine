package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;
import com.jianqu.medicine.other.AppConfig;

import java.util.List;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 可进行拷贝的副本
 */
public final class ListDaiJianApi implements IRequestApi, IRequestServer {

    private String api;

    public ListDaiJianApi setApi(String api) {
        this.api = api;
        return this;
    }

    @Override
    public String getApi() {
        return api;
    }

    private String curState;
    private int isdaijian = 1;
    private int page;
    private int pageSize;
    private String region;
    private String tenantId;
    private String patientPrescriptionNum;
    private String pspnum;
    private String name;
    private String patientName;
    private List<String> times;

    public ListDaiJianApi setCurState(String curState) {
        this.curState = curState;
        return this;
    }

    public ListDaiJianApi setIsdaijian(int isdaijian) {
        this.isdaijian = isdaijian;
        return this;
    }

    public ListDaiJianApi setPage(int page) {
        this.page = page;
        return this;
    }

    public ListDaiJianApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public ListDaiJianApi setRegion(String region) {
        this.region = region;
        return this;
    }

    public ListDaiJianApi setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public ListDaiJianApi setTimes(List<String> times) {
        this.times = times;
        return this;
    }

    public ListDaiJianApi setPatientPrescriptionNum(String patientPrescriptionNum) {
        this.patientPrescriptionNum = patientPrescriptionNum;
        return this;
    }

    public ListDaiJianApi setPspnum(String pspnum) {
        this.pspnum = pspnum;
        return this;
    }

    public ListDaiJianApi setName(String name) {
        this.name = name;
        return this;
    }

    public ListDaiJianApi setPatientName(String patientName) {
        this.patientName = patientName;
        return this;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

    @Override
    public String getHost() {
        return AppConfig.getHostUrl();
    }

    public final static class Bean {
        private String countId;
        private int current;
        private boolean hitCount;
        private int maxLimit;
        private boolean optimizeCountSql;
        private List<String> orders;
        private int pages;
        private boolean searchCount;
        private int size;
        private int total;
        private List<Record> records;

        public String getCountId() {
            return countId;
        }

        public int getCurrent() {
            return current;
        }

        public boolean isHitCount() {
            return hitCount;
        }

        public int getMaxLimit() {
            return maxLimit;
        }

        public boolean isOptimizeCountSql() {
            return optimizeCountSql;
        }

        public List<String> getOrders() {
            return orders;
        }

        public int getPages() {
            return pages;
        }

        public boolean isSearchCount() {
            return searchCount;
        }

        public int getSize() {
            return size;
        }

        public int getTotal() {
            return total;
        }

        public List<Record> getRecords() {
            return records;
        }
    }

    public final static class Record{
        private String address;
        private String age;
        private String casenumber;
        private String curState;
        private String decMethodText;
        private String decSchemeText;
        private int decmothed;
        private int decoctPrice;
        private int decscheme;
        private String delnum;
        private String department;
        private int dhyFlag;
        private String diagresult;
        private int distributionPrice;
        private String doctor;
        private String doperson;
        private int dose;
        private List<String> drug;
        private int drugcount;
        private String dtbTypeText;
        private String dtbaddress;
        private String dtbcompany;
        private String dtbphone;
        private int dtbtype;
        private String expressNo;
        private int expressStatus;
        private String expressStatusDesc;
        private int filetype;
        private String footnote;
        private int getdrugnum;
        private String getdrugtime;
        private String healthcardno;
        private String hospitalkey;
        private String hospitalname;
        private int id;
        private String inpatientarea;
        private int isdaijian;
        private int isrepetition;
        private int jqPresStatus;
        private int labelnum;
        private float money;
        private String name;
        private String orderTime;
        private String outpatientindex;
        private String outpatientnumber;
        private String pTypeText;
        private int packagenum;
        private int particular;
        private String patientfile;
        private int payment;
        private String paymentText;
        private String phone;
        private int procedurejump;
        private String processTypeText;
        private int processtype;
        private String pspnum;
        private int ptype;
        private String remark;
        private String remarka;
        private int sex;
        private int sfFlag;
        private int sfOrderFlag;
        private String sickbed;
        private int soaktime;
        private int soakwater;
        private String takeWayText;
        private String takemethod;
        private int takenum;
        private int takeway;
        private String tenantId;
        private String token;
        private String ward;
        private String yizhu;
        private String dbPicUrl;
        private String fhPicUrl;
        private String jpPicUrl;
        private String jzPicUrl;
        private String tjPicUrl;

        public String getDbPicUrl() {
            return dbPicUrl;
        }

        public String getFhPicUrl() {
            return fhPicUrl;
        }

        public String getJpPicUrl() {
            return jpPicUrl;
        }

        public String getJzPicUrl() {
            return jzPicUrl;
        }

        public String getTjPicUrl() {
            return tjPicUrl;
        }

        public String getAddress() {
            return address;
        }

        public String getAge() {
            return age;
        }

        public String getCasenumber() {
            return casenumber;
        }

        public String getCurState() {
            return curState;
        }

        public String getDecMethodText() {
            return decMethodText;
        }

        public String getDecSchemeText() {
            return decSchemeText;
        }

        public int getDecmothed() {
            return decmothed;
        }

        public int getDecoctPrice() {
            return decoctPrice;
        }

        public int getDecscheme() {
            return decscheme;
        }

        public String getDelnum() {
            return delnum;
        }

        public String getDepartment() {
            return department;
        }

        public int getDhyFlag() {
            return dhyFlag;
        }

        public String getDiagresult() {
            return diagresult;
        }

        public int getDistributionPrice() {
            return distributionPrice;
        }

        public String getDoctor() {
            return doctor;
        }

        public String getDoperson() {
            return doperson;
        }

        public int getDose() {
            return dose;
        }

        public List<String> getDrug() {
            return drug;
        }

        public int getDrugcount() {
            return drugcount;
        }

        public String getDtbTypeText() {
            return dtbTypeText;
        }

        public String getDtbaddress() {
            return dtbaddress;
        }

        public String getDtbcompany() {
            return dtbcompany;
        }

        public String getDtbphone() {
            return dtbphone;
        }

        public int getDtbtype() {
            return dtbtype;
        }

        public String getExpressNo() {
            return expressNo;
        }

        public int getExpressStatus() {
            return expressStatus;
        }

        public String getExpressStatusDesc() {
            return expressStatusDesc;
        }

        public int getFiletype() {
            return filetype;
        }

        public String getFootnote() {
            return footnote;
        }

        public int getGetdrugnum() {
            return getdrugnum;
        }

        public String getGetdrugtime() {
            return getdrugtime;
        }

        public String getHealthcardno() {
            return healthcardno;
        }

        public String getHospitalkey() {
            return hospitalkey;
        }

        public String getHospitalname() {
            return hospitalname;
        }

        public int getId() {
            return id;
        }

        public String getInpatientarea() {
            return inpatientarea;
        }

        public int getIsdaijian() {
            return isdaijian;
        }

        public int getIsrepetition() {
            return isrepetition;
        }

        public int getJqPresStatus() {
            return jqPresStatus;
        }

        public int getLabelnum() {
            return labelnum;
        }

        public float getMoney() {
            return money;
        }

        public String getName() {
            return name;
        }

        public String getOrderTime() {
            return orderTime;
        }

        public String getOutpatientindex() {
            return outpatientindex;
        }

        public String getOutpatientnumber() {
            return outpatientnumber;
        }

        public String getpTypeText() {
            return pTypeText;
        }

        public int getPackagenum() {
            return packagenum;
        }

        public int getParticular() {
            return particular;
        }

        public String getPatientfile() {
            return patientfile;
        }

        public int getPayment() {
            return payment;
        }

        public String getPaymentText() {
            return paymentText;
        }

        public String getPhone() {
            return phone;
        }

        public int getProcedurejump() {
            return procedurejump;
        }

        public String getProcessTypeText() {
            return processTypeText;
        }

        public int getProcesstype() {
            return processtype;
        }

        public String getPspnum() {
            return pspnum;
        }

        public int getPtype() {
            return ptype;
        }

        public String getRemark() {
            return remark;
        }

        public String getRemarka() {
            return remarka;
        }

        public int getSex() {
            return sex;
        }

        public int getSfFlag() {
            return sfFlag;
        }

        public int getSfOrderFlag() {
            return sfOrderFlag;
        }

        public String getSickbed() {
            return sickbed;
        }

        public int getSoaktime() {
            return soaktime;
        }

        public int getSoakwater() {
            return soakwater;
        }

        public String getTakeWayText() {
            return takeWayText;
        }

        public String getTakemethod() {
            return takemethod;
        }

        public int getTakenum() {
            return takenum;
        }

        public int getTakeway() {
            return takeway;
        }

        public String getTenantId() {
            return tenantId;
        }

        public String getToken() {
            return token;
        }

        public String getWard() {
            return ward;
        }

        public String getYizhu() {
            return yizhu;
        }
    }
}