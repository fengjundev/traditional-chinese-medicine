package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 用户登录
 */
public final class LoginApi implements IRequestApi {

    @Override
    public String getApi() {
        return "cmd-auth/token";
    }

    /** 手机号 */
    private String account;
    /** 登录密码 */
    private String password;

    private String grantType;
    private String tenantId;


    public LoginApi setAccount(String account) {
        this.account = account;
        return this;
    }

    public LoginApi setPassword(String password) {
        this.password = password;
        return this;
    }

    public LoginApi setGrantType(String grantType) {
        this.grantType = grantType;
        return this;
    }

    public LoginApi setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }



    public final static class Bean {

        private String accessToken;
        private String account;
        private String avatar;
        private String expiresIn;
        private String refreshToken;
        private String tenantId;
        private String tokenType;
        private String userId;
        private String userName;
        private List<String> roleId;

        public String getAccessToken() {
            return accessToken;
        }

        public String getAccount() {
            return account;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getExpiresIn() {
            return expiresIn;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public String getTenantId() {
            return tenantId;
        }

        public String getTokenType() {
            return tokenType;
        }

        public String getUserId() {
            return userId;
        }

        public String getUserName() {
            return userName;
        }

        public List<String> getRoleId() {
            return roleId;
        }
    }
}