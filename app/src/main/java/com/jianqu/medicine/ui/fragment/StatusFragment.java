package com.jianqu.medicine.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Call;

import com.hjq.base.BaseAdapter;
import com.hjq.http.EasyHttp;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.listener.HttpCallback;
import com.hjq.widget.view.ClearEditText;
import com.hjq.widget.view.FloatActionButton;
import com.jianqu.medicine.R;
import com.jianqu.medicine.app.AppActivity;
import com.jianqu.medicine.app.TitleBarFragment;
import com.jianqu.medicine.http.api.ListApi;
import com.jianqu.medicine.http.api.ListDaiJianApi;
import com.jianqu.medicine.http.model.HttpData;
import com.jianqu.medicine.ui.activity.DetailActivity;
import com.jianqu.medicine.ui.adapter.StatusAdapter;
import com.hjq.widget.layout.WrapRecyclerView;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public final class StatusFragment extends TitleBarFragment<AppActivity>
        implements OnRefreshLoadMoreListener,
        BaseAdapter.OnItemClickListener {

    private SmartRefreshLayout mRefreshLayout;
    private RecyclerView       mRecyclerView;

    private StatusAdapter mAdapter;
    public  int           mType = 1; //0调剂 1复核 2浸泡 3煎煮 4打包(这边指的是页面)

    public  int                            mCurrentPage = 1;//当前页
    private String                         mNum;
    private String                         mName;
    private ClearEditText                  mEtNum;
    private ClearEditText                  mEtName;
    private ActivityResultLauncher<Intent> mLauncher;


    /**
     * 用于获取列表数据的参数
     * 接方0 审核1 调剂2 复核3 泡药4 煎药5 包装6 发货7
     *
     * @return 接口参数(返回当前状态)
     */
    public String formatType() {
        String type = "";
        switch (mType) {
            case 0:
                type = "接方";
                break;
            case 1:
                type = "审核";
                break;
            case 2:
                type = "调剂";
                break;
            case 3:
                type = "复核";
                break;
            case 4:
                type = "泡药";
                break;
            case 5:
                type = "煎药";
                break;
            case 6:
                type = "包装";
                break;
            case 7:
                type = "发货";
                break;
        }
        return type;
    }

    public static StatusFragment newInstance() {
        return new StatusFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.status_fragment;
    }

    @Override
    protected void initView() {
        mLoading = true;

        mRefreshLayout = findViewById(R.id.rl_status_refresh);
        mRecyclerView = findViewById(R.id.rv_status_list);

        findViewById(R.id.scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getParentFragment() != null && getParentFragment() instanceof HomeFragment) {
                    ((HomeFragment) getParentFragment()).startScan();
                }
            }
        });

        mAdapter = new StatusAdapter(getAttachActivity());
        mAdapter.setOnItemClickListener(this);
        //        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
        //            @Override
        //            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        //                super.getItemOffsets(outRect, view, parent, state);
        //                outRect.bottom = 50;
        //            }
        //        });

        mRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshLoadMoreListener(this);

        mEtNum = findViewById(R.id.clear_et_num);
        mEtName = findViewById(R.id.clear_et_name);

        findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNum = mEtNum.getText().toString().trim();
                mName = mEtName.getText().toString().trim();
                mCurrentPage = 1;
                initData();
            }
        });

        findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                initData();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    reset();
                    initData();
                }

            }
        });
    }

    public void reset() {
        if (mEtNum != null && mEtName != null) {
            mNum = "";
            mName = "";
            mEtNum.setText("");
            mEtName.setText("");
            mCurrentPage = 1;
        }
    }

    @Override
    protected void initData() {
        String tenantId = MMKV.defaultMMKV().decodeString("tenantId");
        IRequestApi api;
        if (mType == 3) {
            api = new ListDaiJianApi()
                    .setCurState(formatType())
                    .setPage(mCurrentPage)
                    .setPageSize(10)
                    .setRegion("")
                    .setTenantId(tenantId)
                    .setTimes(new ArrayList<>())
                    .setName(mName)
                    .setPatientName(mName)
                    .setPspnum(mNum)
                    .setPatientPrescriptionNum(mNum)
                    .setApi("cmd-http/queryResentPres");
        } else if (mType == 5) {
            api = new ListApi()
                    .setCurState(formatType())
                    .setPage(mCurrentPage)
                    .setPageSize(10)
                    .setRegion("")
                    .setTenantId(tenantId)
                    .setTimes(new ArrayList<>())
                    .setName(mName)
                    .setPatientName(mName)
                    .setPspnum(mNum)
                    .setPatientPrescriptionNum(mNum)
                    .setApi("cmd-http/queryPackageHisPres");
        } else {
            api = new ListApi()
                    .setCurState(formatType())
                    .setPage(mCurrentPage)
                    .setPageSize(10)
                    .setRegion("")
                    .setTenantId(tenantId)
                    .setTimes(new ArrayList<>())
                    .setName(mName)
                    .setPatientName(mName)
                    .setPspnum(mNum)
                    .setPatientPrescriptionNum(mNum)
                    .setApi("cmd-http/queryResentPres");
        }

        EasyHttp.post(this)
                .api(api)
                .request(new HttpCallback<HttpData<ListApi.Bean>>(this) {
                    @Override
                    public void onStart(Call call) {
                        super.onStart(call);
                    }

                    @Override
                    public void onEnd(Call call) {
                        super.onEnd(call);
                        if (mRefreshLayout.isLoading()) {
                            mRefreshLayout.finishLoadMore();
                        }

                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.finishRefresh();
                        }
                    }

                    @Override
                    public void onSucceed(HttpData<ListApi.Bean> result) {
                        super.onSucceed(result);
                        if (result.isRequestSucceed() && result.getData() != null && result.getData().getRecords() != null) {
                            List<ListApi.Record> records = result.getData().getRecords();
                            if (mCurrentPage == 1) {
                                if (records.size() == 0) {
                                    toast("暂无数据");
                                }
                                mAdapter.clearData();
                                mAdapter.setData(records);
                                mRecyclerView.smoothScrollToPosition(0);
                            } else {
                                mAdapter.addData(records);
                            }

                            mAdapter.setLastPage(mAdapter.getCount() >= result.getData().getTotal());
                            mRefreshLayout.setNoMoreData(mAdapter.isLastPage());
                        }else{
                            toast("暂无数据");
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                    }
                });
    }

    /**
     * {@link BaseAdapter.OnItemClickListener}
     *
     * @param recyclerView RecyclerView对象
     * @param itemView     被点击的条目对象
     * @param position     被点击的条目位置
     */
    @Override
    public void onItemClick(RecyclerView recyclerView, View itemView, int position) {
        DetailActivity.start(getAttachActivity(), mType, mAdapter.getItem(position), mLauncher);
    }

    /**
     * {@link OnRefreshLoadMoreListener}
     */

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        reset();
        postDelayed(this::initData, 1000);
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        mCurrentPage++;
        postDelayed(this::initData, 1000);
    }
}