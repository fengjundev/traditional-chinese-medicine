package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;

/**
 *
 */
public final class GetStatusApi implements IRequestApi {
    private String preNum;
    private String hospitalCode;

    public GetStatusApi setPreNum(String preNum) {
        this.preNum = preNum;
        return this;
    }

    public GetStatusApi setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
        return this;
    }

    @Override
    public String getApi() {
        return "cmd-http/dhy/getPresInfoByPda";
    }
}