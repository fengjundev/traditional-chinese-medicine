package com.jianqu.medicine.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.jianqu.medicine.R;
import com.jianqu.medicine.app.AppAdapter;
import com.jianqu.medicine.app.Utils;
import com.jianqu.medicine.http.api.ListApi;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/09/22
 *    desc   : 状态数据列表
 */
public final class StatusAdapter extends AppAdapter<ListApi.Record> {

    public StatusAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder();
    }

    private final class ViewHolder extends AppAdapter<?>.ViewHolder {


        private final TextView mTv_hospital;
        private final TextView mTv_number;
        private final TextView mTv_name;
        private final TextView mTv_status;
        private final TextView mTv_time;
        private final TextView mTv_doctor;

        private ViewHolder() {
            super(R.layout.status_item);
            mTv_hospital = findViewById(R.id.tv_hospital);
            mTv_number = findViewById(R.id.tv_number);
            mTv_name = findViewById(R.id.tv_name);
            mTv_status = findViewById(R.id.tv_status);
            mTv_time = findViewById(R.id.tv_time);
            mTv_doctor = findViewById(R.id.tv_doctor);
        }

        @Override
        public void onBindView(int position) {
            ListApi.Record item = getItem(position);

            mTv_hospital.setText(Utils.formatStr(item.getHospitalname()));
            mTv_number.setText(Utils.formatStr(item.getPspnum()));
            mTv_name.setText(Utils.formatStr(item.getName()));
            mTv_status.setText(Utils.formatStr(item.getCurState()));
            mTv_time.setText(Utils.formatStr(item.getOrderTime()));
            mTv_doctor.setText(Utils.formatStr(item.getDoctor()));
        }
    }
}