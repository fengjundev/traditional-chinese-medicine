package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 可进行拷贝的副本
 */
public final class createOrderApi implements IRequestApi {

    @Override
    public String getApi() {
        return "/cmd-http/sss/createOrder";
    }

    private String out_order_number;
    private String receiver_name;
    private String receiver_phone;
    private String receiver_address;

    public createOrderApi setOut_order_number(String out_order_number) {
        this.out_order_number = out_order_number;
        return this;
    }

    public createOrderApi setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
        return this;
    }

    public createOrderApi setReceiver_phone(String receiver_phone) {
        this.receiver_phone = receiver_phone;
        return this;
    }

    public createOrderApi setReceiver_address(String receiver_address) {
        this.receiver_address = receiver_address;
        return this;
    }

    public final static class Bean {
        private int code;
        private String out_order_number;
        private String message;

        public int getCode() {
            return code;
        }

        public String getOut_order_number() {
            return out_order_number;
        }

        public String getMessage() {
            return message;
        }
    }
}