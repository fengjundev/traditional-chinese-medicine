package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.hjq.http.config.IRequestType;
import com.hjq.http.model.BodyType;
import com.jianqu.medicine.http.model.RequestServer;

/**
 *
 */
public final class SubmitApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "cmd-http/dhy/updateHisPresStatusByPda";
    }

    private String hospitalName;
    private String operateStartTime;
    private String operatorName;
    private String preNum;
    private String preStatus;
    private String picUrl;
    private String expressNo;
    private String expressCompany;
    private int dtbType;
    private int operateTime;    //煎煮时间或浸泡时间

    public SubmitApi setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
        return this;
    }

    public SubmitApi setOperateStartTime(String operateStartTime) {
        this.operateStartTime = operateStartTime;
        return this;
    }

    public SubmitApi setOperatorName(String operatorName) {
        this.operatorName = operatorName;
        return this;
    }

    public SubmitApi setPreNum(String preNum) {
        this.preNum = preNum;
        return this;
    }

    public SubmitApi setPreStatus(String preStatus) {
        this.preStatus = preStatus;
        return this;
    }

    public SubmitApi setPicUrl(String picUrl) {
        this.picUrl = picUrl;
        return this;
    }

    public SubmitApi setExpressNo(String expressNo) {
        this.expressNo = expressNo;
        return this;
    }

    public SubmitApi setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
        return this;
    }

    public SubmitApi setDtbType(int dtbType) {
        this.dtbType = dtbType;
        return this;
    }

    public void setOperateTime(int operateTime) {
        this.operateTime = operateTime;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

    public final static class Bean {

    }

    @Override
    public String toString() {
        return "SubmitApi{" +
                "hospitalName='" + hospitalName + '\'' +
                ", operateStartTime='" + operateStartTime + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", preNum='" + preNum + '\'' +
                ", preStatus='" + preStatus + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", expressNo='" + expressNo + '\'' +
                ", expressCompany='" + expressCompany + '\'' +
                ", dtbType=" + dtbType +
                ", operateTime=" + operateTime +
                '}';
    }
}