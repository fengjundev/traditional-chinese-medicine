package com.jianqu.medicine.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.IScanListener;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import okhttp3.Call;

import com.example.iscandemo.iScanInterface;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.base.FragmentPagerAdapter;
import com.hjq.http.EasyHttp;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.listener.HttpCallback;
import com.jianqu.medicine.R;
import com.jianqu.medicine.app.AppFragment;
import com.jianqu.medicine.app.TitleBarFragment;
import com.jianqu.medicine.http.api.GetStatusApi;
import com.jianqu.medicine.http.api.ListApi;
import com.jianqu.medicine.http.api.ListDaiJianApi;
import com.jianqu.medicine.http.api.ScanBean;
import com.jianqu.medicine.http.model.HttpData;
import com.jianqu.medicine.ui.activity.DetailActivity;
import com.jianqu.medicine.ui.activity.HomeActivity;
import com.jianqu.medicine.ui.adapter.TabAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/AndroidProject
 * time   : 2018/10/18
 * desc   : 首页 Fragment
 */
public final class HomeFragment extends TitleBarFragment<HomeActivity>
        implements TabAdapter.OnTabListener, ViewPager.OnPageChangeListener {

    private Toolbar mToolbar;


    private RecyclerView mTabView;
    private ViewPager    mViewPager;

    private TabAdapter                           mTabAdapter;
    private FragmentPagerAdapter<AppFragment<?>> mPagerAdapter;

    //iScan接口
    private iScanInterface miScanInterface;

    //数据回调监听
    private final IScanListener miScanListener = new IScanListener() {

        /*
         * param data 扫描数据
         * param type 条码类型
         * param decodeTime 扫描时间
         * param keyDownTime 按键按下的时间
         * param imagePath 图片存储地址，通常用于ocr识别需求（需要先开启保存图片才有效）
         */
        @Override
        public void onScanResults(String data, int type, long decodeTime, long keyDownTime, String imagePath) {

            //解码失败
            if (TextUtils.isEmpty(data)) {
                toast("扫码失败,请重试");
            } else {
                getAttachActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Gson gson = new Gson();
                            ScanBean scanBean = gson.fromJson(data, ScanBean.class);
                            mTvTestCode.setText("扫描结果--" + scanBean.getId() + "--" + scanBean.getHospitalCode() + "--" + System.currentTimeMillis());
                            getStatus(scanBean.getId(), scanBean.getHospitalCode());


                        } catch (Exception e) {
                            e.printStackTrace();
                            toast("扫码失败,请重试");
                        }
                    }
                });

            }

        }
    };

    private TextView mTvTestCode;

    private void getStatus(String id, String hospitalCode) {
        EasyHttp.get(this)
                .api(new GetStatusApi().setPreNum(id).setHospitalCode(hospitalCode))
                .request(new HttpCallback<HttpData<ListApi.Record>>(this) {
                    @Override
                    public void onStart(Call call) {
                        super.onStart(call);
                    }

                    @Override
                    public void onSucceed(HttpData<ListApi.Record> result) {
                        super.onSucceed(result);
                        if (result.isRequestSucceed() && result.getData() != null
                                && !TextUtils.isEmpty(result.getData().getCurState())) {
                            String curState = result.getData().getCurState();
                            int code = getCode(curState);
                            if (code >= 1 && code <= 6) {
                                DetailActivity.start(getAttachActivity(), code, result.getData(), mLauncher);
                            } else {
                                toast("当前处方单不可操作");
                            }

                        } else {
                            toast("未获取到数据");
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                    }

                    @Override
                    public void onEnd(Call call) {
                        super.onEnd(call);
                    }
                });
    }

    private ActivityResultLauncher<Intent> mLauncher;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    ((StatusFragment) mPagerAdapter.getItem(mTabAdapter.getSelectedPosition())).initData();
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        //创建iScanInterface实例化对象
        miScanInterface = new iScanInterface(getAttachActivity());

        //注册iScanInterface 数据回调监听
        miScanInterface.registerScan(miScanListener);

        return R.layout.home_fragment;
    }

    @Override
    protected void initView() {
        mTvTestCode = findViewById(R.id.test_code);
        mToolbar = findViewById(R.id.tb_home_title);

        mTabView = findViewById(R.id.rv_home_tab);
        mViewPager = findViewById(R.id.vp_home_pager);

        mPagerAdapter = new FragmentPagerAdapter<>(this);
        mPagerAdapter.addFragment(StatusFragment.newInstance(), "调剂");
        mPagerAdapter.addFragment(StatusFragment.newInstance(), "复核");
        mPagerAdapter.addFragment(StatusFragment.newInstance(), "浸泡");
        mPagerAdapter.addFragment(StatusFragment.newInstance(), "煎煮");
        mPagerAdapter.addFragment(StatusFragment.newInstance(), "打包");

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        mTabAdapter = new TabAdapter(getAttachActivity());
        mTabView.setAdapter(mTabAdapter);

        // 给这个 ToolBar 设置顶部内边距，才能和 TitleBar 进行对齐
        ImmersionBar.setTitleBar(getAttachActivity(), mToolbar);
    }

    @Override
    protected void initData() {
        mTabAdapter.addItem("调剂");
        mTabAdapter.addItem("复核");
        mTabAdapter.addItem("浸泡");
        mTabAdapter.addItem("煎煮");
        mTabAdapter.addItem("打包");
        mTabAdapter.setOnTabListener(this);

        ((StatusFragment) mPagerAdapter.getItem(0)).initData();
    }

    @Override
    public boolean isStatusBarEnabled() {
        // 使用沉浸式状态栏
        return !super.isStatusBarEnabled();
    }

    /**
     * {@link TabAdapter.OnTabListener}
     */

    @Override
    public boolean onTabSelected(RecyclerView recyclerView, int position) {
        mViewPager.setCurrentItem(position);
        return true;
    }

    /**
     * {@link ViewPager.OnPageChangeListener}
     */

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        if (mTabAdapter == null) {
            return;
        }
        mTabAdapter.setSelectedPosition(position);

        ((StatusFragment) mPagerAdapter.getItem(position)).mType = position + 1;
        ((StatusFragment) mPagerAdapter.getItem(position)).mCurrentPage = 1;
        ((StatusFragment) mPagerAdapter.getItem(position)).reset();
        ((StatusFragment) mPagerAdapter.getItem(position)).initData();

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewPager.setAdapter(null);
        mViewPager.removeOnPageChangeListener(this);
        mTabAdapter.setOnTabListener(null);

        //        注销iScanInterface 数据回调监听
        miScanInterface.unregisterScan(miScanListener);
    }

    public void startScan() {
        /* 开始扫描 */
        miScanInterface.scan_start();
        miScanInterface.setMultiBarEnable(true);
    }

    /**
     * 根据文字获取状态code
     *
     * @return 接口参数(返回当前状态)
     */
    public int getCode(String typeStr) {
        int code = -1;
        switch (typeStr) {
            case "接方":
                code = 0;
                break;
            case "审核":
                code = 1;
                break;
            case "调剂":
                code = 2;
                break;
            case "复核":
                code = 3;
                break;
            case "泡药":
                code = 4;
                break;
            case "煎药":
                code = 5;
                break;
            case "包装":
                code = 6;
                break;
            case "发货":
                code = 7;
                break;
        }
        return code;
    }

}