package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestType;
import com.hjq.http.model.BodyType;

/**
 * 获取煎药、泡药时间
 */
public final class getDateApi implements IRequestApi, IRequestType {

    @Override
    public String getApi() {
        return "cmd-http/dhy/queryOperateTimeById";
    }

    private String tenantId;

    public getDateApi setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }

    public final static class Bean {
        private int decoctTime; //煎药时间
        private int soakTime;   //泡药时间

        public int getDecoctTime() {
            return decoctTime;
        }

        public int getSoakTime() {
            return soakTime;
        }
    }
}