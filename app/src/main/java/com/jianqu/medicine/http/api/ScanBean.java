package com.jianqu.medicine.http.api;

public class ScanBean {
    private String id;  //处方号
    private String hospitalCode;//医院编号

    public ScanBean(String id, String hospitalCode) {
        this.id = id;
        this.hospitalCode = hospitalCode;
    }

    public ScanBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }
}
