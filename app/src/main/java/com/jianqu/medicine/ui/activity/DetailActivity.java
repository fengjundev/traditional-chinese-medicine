package com.jianqu.medicine.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hjq.base.BaseAdapter;
import com.hjq.base.BaseDialog;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.toast.ToastUtils;
import com.hjq.widget.layout.WrapRecyclerView;
import com.jianqu.medicine.R;
import com.jianqu.medicine.app.AppActivity;
import com.jianqu.medicine.app.Utils;
import com.jianqu.medicine.http.api.ListApi;
import com.jianqu.medicine.http.api.SubmitApi;
import com.jianqu.medicine.http.api.UploadPicApi;
import com.jianqu.medicine.http.api.createOrderApi;
import com.jianqu.medicine.http.api.getDateApi;
import com.jianqu.medicine.http.model.HttpData;
import com.jianqu.medicine.ui.adapter.UploadPicAdapter;
import com.jianqu.medicine.ui.dialog.InputDateDialog;
import com.jianqu.medicine.ui.dialog.MessageDialog;
import com.jianqu.medicine.ui.dialog.WaitDialog;
import com.tencent.mmkv.MMKV;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Call;

/**
 *
 */
public final class DetailActivity extends AppActivity {

    private WrapRecyclerView mRvPic;
    private UploadPicAdapter mUploadPicAdapter;

    private static int            mType; //0接方 1审核 2调剂 3复核 4泡药 5煎药 6包装 7发货
    private static ListApi.Record sRecordItem;

    private List<String> mChoosePicList      = new ArrayList<>(); //拍照/选择照片的回调的数据
    private int          mSuccessUploadCount = 0;
    private BaseDialog   mWaitDialog;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// HH:mm:ss
    private int mOperaTime;  //时间（分钟）


    public static void start(Context context, int type, ListApi.Record item, ActivityResultLauncher<Intent> launcher) {
        mType = type;
        sRecordItem = item;
        Intent intent = new Intent(context, DetailActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        launcher.launch(intent);
    }

    public static void start(Context context, int type, ListApi.Record item) {
        mType = type;
        sRecordItem = item;
        Intent intent = new Intent(context, DetailActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.detail_activity;
    }

    @Override
    protected void initView() {
        mWaitDialog = new WaitDialog.Builder(this).setMessage("图片上传中...").create();
        mUploadPicAdapter = new UploadPicAdapter(this, true);
        mUploadPicAdapter.setOnChildClickListener(R.id.iv_close, new BaseAdapter.OnChildClickListener() {
            @Override
            public void onChildClick(RecyclerView recyclerView, View childView, int position) {
                mUploadPicAdapter.removeItem(position);
            }
        });
        mUploadPicAdapter.setOnChildClickListener(R.id.iv, new BaseAdapter.OnChildClickListener() {
            @Override
            public void onChildClick(RecyclerView recyclerView, View childView, int position) {
                ImagePreviewActivity.start(DetailActivity.this, mUploadPicAdapter.getData(), position);
            }
        });
        mUploadPicAdapter.setData(new ArrayList<String>());

        mRvPic = findViewById(R.id.rv_pic);
        mRvPic.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mRvPic.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.right = 40;
            }
        });
        mRvPic.setAdapter(mUploadPicAdapter);

        initDateDialog();

        initPics();

        TextView tv_hospital = findViewById(R.id.tv_hospital);
        TextView tv_number = findViewById(R.id.tv_number);
        TextView tv_name = findViewById(R.id.tv_name);
        TextView tv_sex = findViewById(R.id.tv_sex);
        TextView tv_count = findViewById(R.id.tv_count);
        TextView tv_jian = findViewById(R.id.tv_jian);
        TextView tv_status = findViewById(R.id.tv_status);
        TextView tv_time = findViewById(R.id.tv_time);
        TextView tv_doctor = findViewById(R.id.tv_doctor);
        TextView tv_doctor_remark = findViewById(R.id.tv_doctor_remark);
        TextView tv_info_remark = findViewById(R.id.tv_info_remark);
        TextView tv_image = findViewById(R.id.tv_image);
        LinearLayout ll_image = findViewById(R.id.ll_image);
        Button btn_confirm = findViewById(R.id.btn_confirm);
        Button btn_send = findViewById(R.id.btn_send);

        if (TextUtils.equals(sRecordItem.getCurState(), "包装")) {
            tv_image.setVisibility(View.GONE);
            ll_image.setVisibility(View.GONE);
            btn_send.setVisibility(View.VISIBLE);
            btn_confirm.setVisibility(View.GONE);
        } else {
            tv_image.setVisibility(View.VISIBLE);
            ll_image.setVisibility(View.VISIBLE);
            btn_send.setVisibility(View.GONE);
            btn_confirm.setVisibility(View.VISIBLE);
        }

        // 消息对话框
        MessageDialog.Builder messageDialog = initMessageDialog();
        btn_confirm.setOnClickListener(view -> messageDialog.show());

        // 消息对话框
        MessageDialog.Builder sendDialog = initSendDialog();
        btn_send.setOnClickListener(view -> sendDialog.show());

        findViewById(R.id.iv_add).setOnClickListener(view -> choosePic());

        LinearLayout ll_jian = findViewById(R.id.ll_jian);
        if (mType == 1 || mType == 4) {
            ll_jian.setVisibility(View.VISIBLE);
            tv_jian.setText(sRecordItem.getIsdaijian() == 1 ? "代煎" : "代配");
        } else {
            ll_jian.setVisibility(View.GONE);
        }

        tv_hospital.setText(Utils.formatStr(sRecordItem.getHospitalname()));
        tv_number.setText(Utils.formatStr(sRecordItem.getPspnum()));
        tv_name.setText(Utils.formatStr(sRecordItem.getName()));

        String sexStr;
        int sex = sRecordItem.getSex();
        if (sex == 0) {
            sexStr = "女";
        } else if (sex == 1) {
            sexStr = "男";
        } else {
            sexStr = "保密";
        }
        tv_sex.setText(sexStr);

        tv_count.setText(Utils.formatStr(sRecordItem.getDose() + ""));
        tv_status.setText(Utils.formatStr(sRecordItem.getCurState()));
        tv_time.setText(Utils.formatStr(sRecordItem.getOrderTime()));
        tv_doctor.setText(Utils.formatStr(sRecordItem.getDoctor()));
        tv_doctor_remark.setText(Utils.formatStr(sRecordItem.getFootnote()));
        tv_info_remark.setText(Utils.formatStr(sRecordItem.getRemark()));
    }

    private void initDateDialog() {
        LinearLayout ll_opera_time = findViewById(R.id.ll_opera_time);
        TextView tv_opera_time_left = findViewById(R.id.tv_opera_time_left);
        TextView tv_opera_time_right = findViewById(R.id.tv_opera_time_right);


        String tempType = "";
        //当前是泡药和煎药流程 调用接口获取时间并弹窗
        if ((mType == 5)) {
            tempType = "煎药";
            ll_opera_time.setVisibility(View.VISIBLE);
            tv_opera_time_left.setText("煎药时间：");
        } else if (mType == 4) {
            tempType = "泡药";
            ll_opera_time.setVisibility(View.VISIBLE);
            tv_opera_time_left.setText("泡药时间：");
        } else {
            return;
        }

        String finalTempType = tempType;
        EasyHttp.get(this)
                .api(new getDateApi().setTenantId(sRecordItem.getTenantId()))
                .request(new HttpCallback<HttpData<getDateApi.Bean>>(this) {
                    @Override
                    public void onStart(Call call) {
                        super.onStart(call);
                    }

                    @Override
                    public void onSucceed(HttpData<getDateApi.Bean> result) {
                        super.onSucceed(result);
                        if (result.isRequestSucceed() && result.getData() != null) {
                            getDateApi.Bean data = result.getData();
                            String title = "";
                            if (TextUtils.equals(finalTempType, "煎药")) {
                                title = "设置煎药时间（单位：分钟）";
                                mOperaTime = data.getDecoctTime();
                            } else if (TextUtils.equals(finalTempType, "泡药")) {
                                title = "设置泡药时间（单位：分钟）";
                                mOperaTime = data.getSoakTime();
                            }

                            String operaTimeStr = String.valueOf(mOperaTime);
                            tv_opera_time_right.setText(operaTimeStr);
                            new InputDateDialog.Builder(getActivity())
                                    .setTitle(title)
                                    .setContent(operaTimeStr)
                                    .setHint("请输入数字")
                                    .setConfirm(getString(R.string.common_confirm))
                                    .setCancel(getString(R.string.common_cancel))
                                    .setListener(new InputDateDialog.OnListener() {

                                        @Override
                                        public void onConfirm(BaseDialog dialog, String content) {
                                            mOperaTime = Integer.parseInt(content);
                                            tv_opera_time_right.setText(content);
                                        }
                                    }).show();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                    }

                    @Override
                    public void onEnd(Call call) {
                        super.onEnd(call);
                    }
                });
    }

    @NonNull
    private MessageDialog.Builder initSendDialog() {
        return new MessageDialog.Builder(getActivity())
                // 内容必须要填写
                .setMessage("是否创建订单")
                .setConfirm(getString(R.string.common_confirm))
                .setCancel(getString(R.string.common_cancel))
                // 设置点击按钮后不关闭对话框
                .setAutoDismiss(false)
                .setListener(new MessageDialog.OnListener() {

                    @Override
                    public void onConfirm(BaseDialog dialog) {
                        createOrder();
                    }

                    @Override
                    public void onCancel(BaseDialog dialog) {
                        //取消
                    }
                });
    }

    @NonNull
    private MessageDialog.Builder initMessageDialog() {
        return new MessageDialog.Builder(getActivity())
                // 标题可以不用填写
                //                .setTitle("我是标题")
                // 内容必须要填写
                .setMessage("是否提交当前数据")
                // 确定按钮文本
                .setConfirm(getString(R.string.common_confirm))
                // 设置 null 表示不显示取消按钮
                .setCancel(getString(R.string.common_cancel))
                // 设置点击按钮后不关闭对话框
                //.setAutoDismiss(false)
                .setListener(new MessageDialog.OnListener() {

                    @Override
                    public void onConfirm(BaseDialog dialog) {
                        confirm(false);
                    }

                    @Override
                    public void onCancel(BaseDialog dialog) {
                        //取消
                    }
                });
    }

    private void initPics() {
        String tjPicUrl = sRecordItem.getTjPicUrl();
        if (!TextUtils.isEmpty(tjPicUrl)) {
            findViewById(R.id.ll_tiaoji).setVisibility(View.VISIBLE);
            WrapRecyclerView rvTiaoji = findViewById(R.id.rv_tiaoji);
            UploadPicAdapter tiaojiAdapter = new UploadPicAdapter(this, false);
            tiaojiAdapter.setOnChildClickListener(R.id.iv, (recyclerView, childView, position) -> ImagePreviewActivity.start(DetailActivity.this, tiaojiAdapter.getData(), position));
            rvTiaoji.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            rvTiaoji.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.right = 20;
                }
            });
            String[] split = tjPicUrl.split(",");
            tiaojiAdapter.addData(Arrays.asList(split));
            rvTiaoji.setAdapter(tiaojiAdapter);
        }

        String fhPicUrl = sRecordItem.getFhPicUrl();
        if (!TextUtils.isEmpty(fhPicUrl)) {
            findViewById(R.id.ll_fuhe).setVisibility(View.VISIBLE);
            WrapRecyclerView rvfuhe = findViewById(R.id.rv_fuhe);
            UploadPicAdapter fuheAdapter = new UploadPicAdapter(this, false);
            fuheAdapter.setOnChildClickListener(R.id.iv, (recyclerView, childView, position) -> ImagePreviewActivity.start(DetailActivity.this, fuheAdapter.getData(), position));
            rvfuhe.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            rvfuhe.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.right = 20;
                }
            });
            String[] split = fhPicUrl.split(",");
            fuheAdapter.addData(Arrays.asList(split));
            rvfuhe.setAdapter(fuheAdapter);
        }

        String jpPicUrl = sRecordItem.getJpPicUrl();
        if (!TextUtils.isEmpty(jpPicUrl)) {
            findViewById(R.id.ll_jinpao).setVisibility(View.VISIBLE);
            WrapRecyclerView rvjinpao = findViewById(R.id.rv_jinpao);
            UploadPicAdapter jinpaoAdapter = new UploadPicAdapter(this, false);
            jinpaoAdapter.setOnChildClickListener(R.id.iv, (recyclerView, childView, position) -> ImagePreviewActivity.start(DetailActivity.this, jinpaoAdapter.getData(), position));
            rvjinpao.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            rvjinpao.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.right = 20;
                }
            });
            String[] split = jpPicUrl.split(",");
            jinpaoAdapter.addData(Arrays.asList(split));
            rvjinpao.setAdapter(jinpaoAdapter);
        }

        String jzPicUrl = sRecordItem.getJzPicUrl();
        if (!TextUtils.isEmpty(jzPicUrl)) {
            findViewById(R.id.ll_jianzhu).setVisibility(View.VISIBLE);
            WrapRecyclerView rvjianzhu = findViewById(R.id.rv_jianzhu);
            UploadPicAdapter jianzhuAdapter = new UploadPicAdapter(this, false);
            jianzhuAdapter.setOnChildClickListener(R.id.iv, (recyclerView, childView, position) -> ImagePreviewActivity.start(DetailActivity.this, jianzhuAdapter.getData(), position));
            rvjianzhu.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            rvjianzhu.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.right = 20;
                }
            });
            String[] split = jzPicUrl.split(",");
            jianzhuAdapter.addData(Arrays.asList(split));
            rvjianzhu.setAdapter(jianzhuAdapter);
        }


    }

    /**
     * 用于获取列表数据的参数
     * 接方0 审核1 调剂2 复核3 泡药4 煎药5 包装6 发货7
     *
     * @return 接口参数(返回当前状态的下一个状态)
     */
    public String formatType() {
        String type = "";
        switch (mType) {
            case 0:
                type = "审核";
                break;
            case 1:
                type = "调剂";
                break;
            case 2:
                type = "复核";
                break;
            case 3:
                if (sRecordItem.getIsdaijian() == 1) {
                    type = "泡药";
                } else if (sRecordItem.getIsdaijian() == 0) {
                    type = "包装";
                }
                //                原先业务逻辑
                //                type = "泡药";
                break;
            case 4:
                type = "煎药";
                break;
            case 5:
                type = "包装";
                break;
            case 6:
                type = "发货";
                break;
        }
        return type;
    }

    /**
     * 创建订单
     */
    private void createOrder() {
        EasyHttp.post(this)
                .api(new createOrderApi()
                        .setOut_order_number(sRecordItem.getPspnum())
                        .setReceiver_name(sRecordItem.getName())
                        .setReceiver_phone(sRecordItem.getDtbphone())
                        .setReceiver_address(sRecordItem.getDtbaddress()))
                .request(new HttpCallback<HttpData<createOrderApi.Bean>>(this) {
                    @Override
                    public void onStart(Call call) {
                        super.onStart(call);
                    }

                    @Override
                    public void onSucceed(HttpData<createOrderApi.Bean> result) {
                        super.onSucceed(result);
                        if (result.getData().getCode() == 0) {
                            //成功
                            confirm(true);
                        } else {
                            ToastUtils.show(result.getData().getMessage());
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                    }

                    @Override
                    public void onEnd(Call call) {
                        super.onEnd(call);
                    }
                });
    }

    private void confirm(boolean isCreateOrder) {
        SubmitApi submitApi = new SubmitApi();
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        if (isCreateOrder) {
            submitApi.setPreStatus("发货")
                    .setExpressNo(sRecordItem.getExpressNo())
                    .setExpressCompany("闪时送")
                    .setDtbType(11);
        } else {
            if (mType == 0 && mUploadPicAdapter.getCount() == 0) {
                toast("请先上传图片");
                return;
            }

            StringBuilder picUrl = new StringBuilder();
            int size = mUploadPicAdapter.getData().size();
            for (int i = 0; i < size; i++) {
                picUrl.append(mUploadPicAdapter.getData().get(i));
                if (i != size - 1) {
                    picUrl.append(",");
                }
            }

            submitApi.setPreStatus(formatType())
                    .setPicUrl(picUrl.toString());
        }

        submitApi.setHospitalName(sRecordItem.getHospitalname())
                .setOperatorName(MMKV.defaultMMKV().decodeString("userName"))
                .setPreNum(sRecordItem.getPspnum())
                .setOperateStartTime(simpleDateFormat.format(date))
                .setOperateTime(mOperaTime);

        EasyHttp.post(this)
                .api(submitApi)
                .request(new HttpCallback<HttpData<Boolean>>(this) {
                    @Override
                    public void onStart(Call call) {
                        super.onStart(call);
                    }

                    @Override
                    public void onSucceed(HttpData<Boolean> result) {
                        super.onSucceed(result);
                        if (result.isRequestSucceed()) {
                            toast("提交成功");
                            postDelayed(() -> {
                                Intent intent = new Intent();
                                setResult(RESULT_OK, intent);
                                finish();
                            }, 1000);
                        } else {
                            Toast.makeText(getContext(), result.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                    }

                    @Override
                    public void onEnd(Call call) {
                        super.onEnd(call);
                    }
                });
    }

    private void choosePic() {
        ImageSelectActivity.start(DetailActivity.this, 6, new ImageSelectActivity.OnPhotoSelectListener() {

            @Override
            public void onSelected(List<String> data) {
                if (data != null && data.size() > 0) {
                    mWaitDialog.show();
                    mChoosePicList = data;
                    mSuccessUploadCount = 0;
                    for (String item : data) {
                        uploadPic(item);
                    }
                }
            }

            @Override
            public void onCancel() {
                toast("取消了");
            }
        });
    }

    private void uploadPic(String item) {
        EasyHttp.post(this)
                .api(new UploadPicApi()
                        .setFile(new File(item)))
                .request(new HttpCallback<HttpData<String>>(this) {
                    @Override
                    public void onStart(Call call) {
                        //                        super.onStart(call);
                    }

                    @Override
                    public void onSucceed(HttpData<String> result) {
                        super.onSucceed(result);
                        if (result.isRequestSucceed()) {
                            mSuccessUploadCount++;
                            mUploadPicAdapter.addItem(result.getData());
                        } else {
                            mWaitDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        super.onFail(e);
                        mWaitDialog.dismiss();
                    }

                    @Override
                    public void onEnd(Call call) {
                        //                        super.onEnd(call);
                        if (mSuccessUploadCount == mChoosePicList.size() && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                    }
                });
    }

    @Override
    protected void initData() {

    }
}