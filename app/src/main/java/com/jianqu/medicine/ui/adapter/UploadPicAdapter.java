package com.jianqu.medicine.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jianqu.medicine.R;
import com.jianqu.medicine.app.AppAdapter;

import androidx.annotation.NonNull;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/09/22
 *    desc   : 状态数据列表
 */
public final class UploadPicAdapter extends AppAdapter<String> {

    private boolean mIsUpload;

    public UploadPicAdapter(Context context, boolean isUpload) {
        super(context);
        mIsUpload = isUpload;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder();
    }

    private final class ViewHolder extends AppAdapter<?>.ViewHolder {

        private final ImageView mIv;
        private final ImageView mIv_close;

        private ViewHolder() {
            super(R.layout.upload_pic_item);
            mIv = findViewById(R.id.iv);
            mIv_close = findViewById(R.id.iv_close);

        }

        @Override
        public void onBindView(int position) {
            String item = getItem(position);
            Glide.with(getContext()).load(item).error(R.drawable.image_loading_ic).into(mIv);

            mIv_close.setVisibility(mIsUpload ? View.VISIBLE : View.GONE);
        }
    }
}