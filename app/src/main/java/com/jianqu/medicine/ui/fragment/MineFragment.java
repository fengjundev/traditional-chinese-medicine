package com.jianqu.medicine.ui.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.http.model.FileContentResolver;
import com.hjq.widget.layout.SettingBar;
import com.jianqu.medicine.R;
import com.jianqu.medicine.aop.SingleClick;
import com.jianqu.medicine.app.TitleBarFragment;
import com.jianqu.medicine.app.Utils;
import com.jianqu.medicine.http.api.UpdateImageApi;
import com.jianqu.medicine.http.glide.GlideApp;
import com.jianqu.medicine.http.model.HttpData;
import com.jianqu.medicine.ui.activity.AboutActivity;
import com.jianqu.medicine.ui.activity.BrowserActivity;
import com.jianqu.medicine.ui.activity.DialogActivity;
import com.jianqu.medicine.ui.activity.GuideActivity;
import com.jianqu.medicine.ui.activity.HomeActivity;
import com.jianqu.medicine.ui.activity.ImageCropActivity;
import com.jianqu.medicine.ui.activity.ImagePreviewActivity;
import com.jianqu.medicine.ui.activity.ImageSelectActivity;
import com.jianqu.medicine.ui.activity.LoginActivity;
import com.jianqu.medicine.ui.activity.PasswordForgetActivity;
import com.jianqu.medicine.ui.activity.PasswordResetActivity;
import com.jianqu.medicine.ui.activity.PersonalDataActivity;
import com.jianqu.medicine.ui.activity.PhoneResetActivity;
import com.jianqu.medicine.ui.activity.RegisterActivity;
import com.jianqu.medicine.ui.activity.SettingActivity;
import com.jianqu.medicine.ui.activity.StatusActivity;
import com.jianqu.medicine.ui.activity.VideoPlayActivity;
import com.jianqu.medicine.ui.activity.VideoSelectActivity;
import com.jianqu.medicine.ui.dialog.AddressDialog;
import com.jianqu.medicine.ui.dialog.InputDialog;
import com.jianqu.medicine.ui.dialog.MessageDialog;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mmkv.MMKV;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2018/10/18
 *    desc   : 我的 Fragment
 */
public final class MineFragment extends TitleBarFragment<HomeActivity> {

    private ViewGroup  mAvatarLayout;
    private ImageView  mAvatarView;
    private SettingBar mIdView;
    private SettingBar mNameView;

    /** 头像地址 */
    private Uri mAvatarUrl;
    private Button mBtnExit;
    private String mUserName;
    private String mHeadPic;
    private String mAccount;

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.personal_data_activity;
    }

    @Override
    protected void initView() {
        MMKV mmkv = MMKV.defaultMMKV();
        mUserName = mmkv.decodeString("userName");
        mHeadPic = mmkv.decodeString("headPic");
        mAccount = mmkv.decodeString("account");

        mAvatarLayout = findViewById(R.id.fl_person_data_avatar);
        mAvatarView = findViewById(R.id.iv_person_data_avatar);
        mIdView = findViewById(R.id.sb_person_data_id);
        mNameView = findViewById(R.id.sb_person_data_name);
        mBtnExit = findViewById(R.id.btn_exit);
//        setOnClickListener(mAvatarLayout, mAvatarView, mNameView, mBtnExit);
        setOnClickListener(mBtnExit);
    }

    @Override
    protected void initData() {
        GlideApp.with(getActivity())
                .load(TextUtils.isEmpty(mHeadPic) ? R.drawable.avatar_placeholder_ic : mHeadPic)
                .placeholder(R.drawable.avatar_placeholder_ic)
                .error(R.drawable.avatar_placeholder_ic)
                .transform(new MultiTransformation<>(new CenterCrop(), new CircleCrop()))
                .into(mAvatarView);

        mIdView.setRightText(Utils.formatStr(mAccount));
        mNameView.setRightText(Utils.formatStr(mUserName));

    }

    @SingleClick
    @Override
    public void onClick(View view) {
        if (view == mAvatarLayout) {
            ImageSelectActivity.start(getAttachActivity(), data -> {
                // 裁剪头像
                cropImageFile(new File(data.get(0)));
            });
        } else if (view == mAvatarView) {
            if (mAvatarUrl != null) {
                // 查看头像
                ImagePreviewActivity.start(getActivity(), mAvatarUrl.toString());
            } else {
                // 选择头像
                onClick(mAvatarLayout);
            }
        } else if (view == mNameView) {
            new InputDialog.Builder(getAttachActivity())
                    // 标题可以不用填写
                    .setTitle(getString(R.string.personal_data_name_hint))
                    .setContent(mNameView.getRightText())
                    //.setHint(getString(R.string.personal_data_name_hint))
                    //.setConfirm("确定")
                    // 设置 null 表示不显示取消按钮
                    //.setCancel("取消")
                    // 设置点击按钮后不关闭对话框
                    //.setAutoDismiss(false)
                    .setListener((dialog, content) -> {
                        if (!mNameView.getRightText().equals(content)) {
                            mNameView.setRightText(content);
                        }
                    })
                    .show();
        } else if (view == mBtnExit) {
            MMKV mmkv = MMKV.defaultMMKV();
            mmkv.clearAll();
            EasyConfig.getInstance().removeHeader("Cmd-Auth");

            LoginActivity.start(getAttachActivity(), "", "");
            finish();
        }
    }

    /**
     * 裁剪图片
     */
    private void cropImageFile(File sourceFile) {
        ImageCropActivity.start(getAttachActivity(), sourceFile, 1, 1, new ImageCropActivity.OnCropListener() {

            @Override
            public void onSucceed(Uri fileUri, String fileName) {
                File outputFile;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    outputFile = new FileContentResolver(getActivity(), fileUri, fileName);
                } else {
                    try {
                        outputFile = new File(new URI(fileUri.toString()));
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        outputFile = new File(fileUri.toString());
                    }
                }
                updateCropImage(outputFile, true);
            }

            @Override
            public void onError(String details) {
                // 没有的话就不裁剪，直接上传原图片
                // 但是这种情况极其少见，可以忽略不计
                updateCropImage(sourceFile, false);
            }
        });
    }

    /**
     * 上传裁剪后的图片
     */
    private void updateCropImage(File file, boolean deleteFile) {
        if (true) {
            if (file instanceof FileContentResolver) {
                mAvatarUrl = ((FileContentResolver) file).getContentUri();
            } else {
                mAvatarUrl = Uri.fromFile(file);
            }
            GlideApp.with(getActivity())
                    .load(mAvatarUrl)
                    .transform(new MultiTransformation<>(new CenterCrop(), new CircleCrop()))
                    .into(mAvatarView);
            return;
        }

        EasyHttp.post(this)
                .api(new UpdateImageApi()
                        .setImage(file))
                .request(new HttpCallback<HttpData<String>>(this) {

                    @Override
                    public void onSucceed(HttpData<String> data) {
                        mAvatarUrl = Uri.parse(data.getData());
                        GlideApp.with(getActivity())
                                .load(mAvatarUrl)
                                .transform(new MultiTransformation<>(new CenterCrop(), new CircleCrop()))
                                .into(mAvatarView);
                        if (deleteFile) {
                            file.delete();
                        }
                    }
                });
    }

    @Override
    public boolean isStatusBarEnabled() {
        // 使用沉浸式状态栏
        return !super.isStatusBarEnabled();
    }
}