package com.jianqu.medicine.http.api;

import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;

import java.io.File;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 可进行拷贝的副本
 */
public final class UploadPicApi implements IRequestApi {
    private File file;

    @Override
    public String getApi() {
        return "cmd-http/uploadFile";
    }

    public UploadPicApi setFile(File file) {
        this.file = file;
        return this;
    }

    public final static class Bean {

    }
}